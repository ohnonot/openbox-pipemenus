# A collection of Openbox pipemenus

## recent

A recent files pipemenu with icons (optional).

The bash script

- sorts the list by access time and updates the entry in recently-used.xbel when you open a file with it, i.e. next time you will see that file at the top of the list.
- filters out files that do not exist (anymore) or are unreadable for other reasons.
- offers to open the file in question with all the applications listed in recently-used.xbel after checking they are present in PATH, and additionally with a mime-opener like 'xdg-open'.
- assigns icons from the current icon theme and its Inherits, as close as possible to how e.g. a filemanager does it.
- caches icon paths and icons so that subsequent runs are much faster.
- uses an absolute minimum of external commands & dependencies.

The script also has a `-h` option, please execute that in a terminal for a full description.

__Dependencies__

`bash`, `xmlstarlet` and optionally `gio` (glib2, libglib2.0-bin) if you use icons.

## transmission-ob

Pipemenu to access transmission-cli's daemon and (web) interface.

The Transmission BitTorrent client has a daemon that can be accessed with a (web) interface.  
That means no resources are wasted on the graphical user interface, unless you explicitely open it (in 
a browser).  
The web interface is part of the package, but other frontends exist, e.g. [transgui][tg].

If you have authentication enabled you can tell your shell to set an environment variable
called `TR_AUTH` (`man transmission-remote`) (be aware that this is not very secure).
This script will recognize it and use it, no need to enter username/password anymore.

__Dependencies__

Requires `bash`, `transmission-cli` and `timeout` (coreutils).  
On archlinux, `transmission-daemon` is included in `transmission-cli`, but on
debian (stable) it is a separate package.  

Optionally:

- a notification daemon that provides the `notify-send` executable
- one of `urxvt` or `xterm` (other terminals not tested)
- `xclip` to add torrents from the clipboard
- `sed` for listing stats in a terminal

The script also has a `-h` option, please execute that in a terminal for a full description.

## How to install/use pipemenus

See [Openbox wiki][1].

[1]: http://openbox.org/wiki/Help:Menus#Pipe_menus
[2]: https://notabug.org/ohnonot/transmission-web
[tg]: https://github.com/transmission-remote-gui/transgui/
